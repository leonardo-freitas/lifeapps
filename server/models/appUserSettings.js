const mongoose = require('mongoose');

module.exports =  function(){

    var appUserSettingsSchema = mongoose.Schema({
        user: { type: mongoose.Schema.ObjectId, ref: 'User' },
        app: String,
        settings: mongoose.Schema.Types.Mixed
    });

    return mongoose.model('AppUserSettings', appUserSettingsSchema);
};
