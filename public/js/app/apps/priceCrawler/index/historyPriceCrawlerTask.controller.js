(function () {
    angular
        .module('lifeapps')
        .controller('HistoryPriceCrawlerTaskController', Controller);
    Controller.$inject = ['$mdDialog', 'price', 'task'];
    function Controller($mdDialog, price, task) {
        var vm = this;

        vm.task = task;
        vm.history = angular.copy(task.history);
        vm.order = "-date";
        
        vm.close = function(){
            $mdDialog.cancel();
        };

        vm.reorder = function(){
            var compare;
            var major = 1, minor = -1;

            var order = vm.order;
            if (vm.order.contains("-")){
                major = -1;
                minor = 1;
                order = vm.order.substring(1);
            }

            if (order === "date"){
                compare = function(a, b) {
                    var dateA = new Date(a.date);
                    var dateB = new Date(b.date);
                    if (dateA.getTime() < dateB.getTime()){
                        return major;
                    }
                    if (dateA.getTime() > dateB.getTime()){
                        return minor;
                    }
                    return 0;
                }
            }
            else{
                compare = function(a, b) {
                    var priceA = price.priceToNumber(a.data[order]);
                    var priceB = price.priceToNumber(b.data[order]);
                    if (!priceA){
                        return -1;
                    }
                    else if (!priceB){
                        return 1;
                    }
                    else if (priceA < priceB) {
                        return major;
                    }
                    else if (priceA > priceB) {
                        return minor;
                    }
                    return 0;
                }
            }
            vm.history.sort(compare);
        }
    }
})();