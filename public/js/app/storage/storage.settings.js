(function() {
    angular
        .module('lifeapps')
        .constant('storageSettings', {
            default_storage: "localStorage"
        });
})();
