(function() {
    angular
        .module('lifeapps')
        .constant('appsSettings', {
            apps: [
                {
                    name: "apps.priceCrawler.name",
                    description: "apps.priceCrawler.description",
                    image: "images/crawler.jpg",
                    path: "/priceCrawler/index",
                    permissions: ["user"]
                }
            ]
        });
})();
