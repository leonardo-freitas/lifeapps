var CronJob = require('cron').CronJob;
var priceCrawlerStack = require('../services/priceCrawlerStack');
var dateMethods = require('../services/dateMethods');
var emailAgent = require('../services/emailAgent');
var price = require('../services/price');
var i18n = require('../locale/i18n')("ptBR");

var intervalMinutes = {
    "5m": 5,
    "10m": 10,
    "30m": 30,
    "1h": 60,
    "2h": 120,
    "12h": 720,
    "1d": 1440,
    "1w": 10080
};

module.exports = function (app) {
    var PriceCrawlerTask = app.models.priceCrawlerTask;

    return new CronJob('00 */5 * * * *', function() {
        var now = new Date();
        PriceCrawlerTask.find({}).populate("user").exec(function(err, tasks){
            tasks.forEach(function(task){
                if (!task.lastCrawling ||
                    dateMethods.minutesDiff(now, task.lastCrawling) >= intervalMinutes[task.interval]){
                    priceCrawlerStack.push(task.url, task.crawlerType, function(error, prices){
                        task.history.push({
                            data: prices,
                            date: now
                        });
                        task.lastCrawling = now;
                        task.save();
                        if (task.history.length > 1){
                            var oldPriceObj = task.history[task.history.length - 2].data;
                            var oldPrice = oldPriceObj.normalPrice;
                            var newPrice = prices.normalPrice;
                            if (oldPriceObj.promoPrice && prices.promoPrice){
                                oldPrice = oldPriceObj.promoPrice;
                                newPrice = prices.promoPrice;
                            }
                            if (task.notifyLowerPrice && price.priceToNumber(newPrice) < price.priceToNumber(oldPrice)){
                                emailAgent.sendEmail(
                                    task.user.email,
                                    i18n.translate("newPriceEmail.subject", {taskName: task.name}),
                                    i18n.translate("newPriceEmail.html", {
                                        taskName: task.name,
                                        taskLink: task.url,
                                        oldPrice: oldPrice,
                                        newPrice: newPrice
                                    })
                                )
                            }
                        }
                    });
                }
            });
        });
    }, null, true, 'America/Sao_Paulo');
};
