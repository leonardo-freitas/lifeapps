(function() {
    angular
        .module('lifeapps')
        .factory('PriceCrawlerTask', Service);
    Service.$inject = ['$resource'];
    function Service($resource) {
        var UserResource = $resource(
            '/api/priceCrawlerTask/:id',
            {},
            {}
        );

        return UserResource;
    }
})();
