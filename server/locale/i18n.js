function getDescendantProp(obj, desc) {
    var arr = desc.split(".");
    while(arr.length && (obj = obj[arr.shift()])){
        if (!obj)
            return null;
    }
    return obj;
}

module.exports = function(locale) {
	return {
		translationFile: require("./" + locale),
		translate: function(msg, param){
			var i18nMsg = getDescendantProp(this.translationFile, msg);
            if (!i18nMsg)
                i18nMsg = msg;
			for (var prop in param){
				i18nMsg = i18nMsg.replace(new RegExp("{{" + prop + "}}", "g"), param[prop]);
			}
			return i18nMsg;
		}
	};
};
