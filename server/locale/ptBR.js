module.exports = {
	newPriceEmail: {
		subject: "Redução de Preço em {{taskName}}! - The Life Apps",
		html: "Nosso sistema detectou uma queda no preço de {{taskName}}<br>O preço caiu de  {{oldPrice}} para {{newPrice}}!<br>Link para comprar: <a href='{{taskLink}}'>{{taskLink}}</a></a><br>Atenciosamente,<br>Equipe The Life Apps."
	},
	inviteEmail: {
		subject: "Convite - The Life Apps",
		html: "Prezada(o),<br><br>Você foi convidada(o) para a plataforma The Life Apps. Tenha um bom uso de nossos aplicativos para tornar sua vida mais fácil e ágil!<br><br>Sua senha o é: {{token}}<br><br>Apesar de totalmente segura, recomendamos que você troque por uma senha pessoal para facilmente memoriza-la.<br><br>Atenciosamente,<br>The Life Apps<br><a href='www.thelifeapps.com'>www.thelifeapps.com</a>"
	}
};
