var phridge = require('phridge');

const crawlerSettings = {
    defaultOptions: {
        loadImages: false
    },
    maxTimeout: 60000
};

module.exports = {
    kabumScrapper: function(url, callback){
        phridge.spawn(crawlerSettings.defaultOptions).then(function (phantom) {
            phantom.run(url, crawlerSettings.maxTimeout, function (target, maxTimeout, resolve) {

                var page = webpage.create();
                page.open(target, function () {
                    var priceDisplayed;
                    var scrapping = false;
                    var checkStateInterval = setInterval(function(){
                        priceDisplayed = page.evaluate(function () {
                            return document.getElementsByClassName("preco_desconto").length > 0;
                        });
                        if (priceDisplayed){
                            getPrices();
                            clearInterval(checkStateInterval);
                        }
                    }, 100);

                    function getPrices(){
                        scrapping = true;
                        var prices = page.evaluate(function () {
                            var oldPrice = null;
                            if (document.getElementsByClassName("preco_antigo").length){
                                oldPrice = document.getElementsByClassName("preco_antigo")[0].textContent;
                                oldPrice = oldPrice.substring(3, oldPrice.length - 4)
                            }
                            var normalPrice = document.getElementsByClassName("preco_normal")[0].textContent;
                            normalPrice = normalPrice.substring(normalPrice.indexOf("R$"), normalPrice.indexOf("\n", normalPrice.indexOf("R$")));
                            var promoPrice = document.getElementsByClassName("preco_desconto")[0].children[0].textContent;
                            return {
                                oldPrice: oldPrice,
                                normalPrice: normalPrice,
                                promoPrice: promoPrice
                            };
                        });

                        resolve({error: null, prices: prices});
                    }

                    setTimeout(function(){
                        if (!scrapping){
                            clearInterval(checkStateInterval);
                            resolve({error: "timeout", prices: []});
                        }
                    }, maxTimeout);
                });
            }).then(function (response) {
                callback(response.error, response.prices);
            });
        });
    },
    americanasSubmarinoScrapper: function(url, callback){
        phridge.spawn(crawlerSettings.defaultOptions).then(function (phantom) {
            phantom.run(url, crawlerSettings.maxTimeout, function (target, maxTimeout, resolve) {

                var page = webpage.create();
                page.open(target, function () {
                    var priceDisplayed;
                    var scrapping = false;
                    var checkStateInterval = setInterval(function(){
                        priceDisplayed = page.evaluate(function () {
                            return document.getElementsByClassName("sales-price").length > 0;
                        });
                        if (priceDisplayed){
                            getPrices();
                            clearInterval(checkStateInterval);
                        }
                    }, 100);

                    function getPrices(){
                        scrapping = true;
                        var prices = page.evaluate(function () {
                            var normalPrice = document.getElementsByClassName("sales-price")[0].textContent;
                            var promoPrice = null;
                            if (document.getElementsByClassName("payment-option boleto ").length){
                                promoPrice = document.getElementsByClassName("payment-option boleto ")[0].children[0].children[1].children[0].textContent;
                            }
                            return {
                                normalPrice: normalPrice,
                                promoPrice: promoPrice
                            };
                        });

                        resolve({error: null, prices: prices});
                    }

                    setTimeout(function(){
                        if (!scrapping){
                            clearInterval(checkStateInterval);
                            resolve({error: "timeout", prices: []});
                        }
                    }, maxTimeout);
                });
            }).then(function (response) {
                callback(response.error, response.prices);
            });
        });
    },
    GPAScrapper: function(url, callback){
        phridge.spawn(crawlerSettings.defaultOptions).then(function (phantom) {
            phantom.run(url, crawlerSettings.maxTimeout, function (target, maxTimeout, resolve) {

                var page = webpage.create();
                page.open(target, function () {
                    var priceDisplayed;
                    var scrapping = false;
                    var checkStateInterval = setInterval(function(){
                        priceDisplayed = page.evaluate(function () {
                            return document.getElementById("ctl00_Conteudo_ctl01_precoPorValue");
                        });
                        if (priceDisplayed){
                            getPrices();
                            clearInterval(checkStateInterval);
                        }
                    }, 100);

                    function getPrices(){
                        scrapping = true;
                        var prices = page.evaluate(function () {
                            var normalPrice = document.getElementById("ctl00_Conteudo_ctl01_precoPorValue").textContent;
                            return {
                                normalPrice: normalPrice
                            };
                        });

                        resolve({error: null, prices: prices});
                    }

                    setTimeout(function(){
                        if (!scrapping){
                            clearInterval(checkStateInterval);
                            resolve({error: "timeout", prices: []});
                        }
                    }, maxTimeout);
                });
            }).then(function (response) {
                callback(response.error, response.prices);
            });
        });
    }
};