(function () {
    angular
        .module('lifeapps')
        .controller('InviteController', Controller);
    Controller.$inject = ['userService', 'notifier'];
    function Controller(userService, notifier) {
        var vm = this;

        vm.sendInvite = function(){
            if (vm.inviteForm.$valid) {
                userService.sendInvite(vm.email).then(function(){
                        notifier.success("invite.inviteSuccess");
                        delete vm.email;
                    },
                    function(reason){
                        notifier.error(reason);
                    })
            }
        };
 
    }
})();