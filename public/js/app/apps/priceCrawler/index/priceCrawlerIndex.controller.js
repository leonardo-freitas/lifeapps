(function () {
    angular
        .module('lifeapps')
        .controller('PriceCrawlerIndexController', Controller);
    Controller.$inject = ['$mdDialog', 'priceCrawlerTaskService', 'notifier', 'translateFilter'];
    function Controller($mdDialog, priceCrawlerTaskService, notifier, translateFilter) {
        var vm = this;

        vm.addTask = function($event) {
            $mdDialog.show({
                    attachTo: angular.element(document.body),
                    controller: 'NewPriceCrawlerTaskController',
                    controllerAs: 'dialog',
                    templateUrl: '/partials/apps/priceCrawler/newTask',
                    parent: angular.element(document.body),
                    targetEvent: $event,
                    clickOutsideToClose: true
                })
                .then(function() {
                    loadCrawlerTasks();
                });
        };

        vm.updateTask = function($event, task){
            $mdDialog.show({
                    attachTo: angular.element(document.body),
                    controller: 'UpdatePriceCrawlerTaskController',
                    controllerAs: 'dialog',
                    templateUrl: '/partials/apps/priceCrawler/updateTask',
                    parent: angular.element(document.body),
                    targetEvent: $event,
                    clickOutsideToClose: true,
                    locals: {
                        task: task
                    }
                })
                .then(function() {
                    loadCrawlerTasks();
                });
        };

        vm.deleteTask = function($event, task){
            var confirm = $mdDialog.confirm()
                .title(translateFilter('apps.priceCrawler.deleteTaskTitle',
                    {task: task.name, crawler: translateFilter('apps.priceCrawler.crawlers.' + task.crawlerType)}))
                .textContent(translateFilter('apps.priceCrawler.deleteTaskDescription'))
                .ariaLabel('Delete task')
                .targetEvent($event)
                .ok(translateFilter('common.remove'))
                .cancel(translateFilter('common.cancel'));

            $mdDialog.show(confirm).then(function() {
                priceCrawlerTaskService.deleteCrawlerTask(task._id).then(
                    function () {
                        notifier.success('apps.priceCrawler.deleteTaskSuccess');
                        loadCrawlerTasks();
                    },
                    function(reason){
                        notifier.error(reason);
                    }
                );
            });
        };

        vm.historyTask = function($event, task){
            $mdDialog.show({
                    attachTo: angular.element(document.body),
                    controller: 'HistoryPriceCrawlerTaskController',
                    controllerAs: 'dialog',
                    templateUrl: '/partials/apps/priceCrawler/historyTask',
                    parent: angular.element(document.body),
                    targetEvent: $event,
                    clickOutsideToClose: true,
                    locals: {
                        task: task
                    }
                })
                .then(function() {
                    loadCrawlerTasks();
                });
        };

        vm.getLastHistoryPrice = function(task){
            if (!task.history || !task.history.length){
                return null;
            }
            return task.history[task.history.length - 1].data;
        };

        function loadCrawlerTasks(){
            vm.loadingTasks = true;
            delete vm.tasks;
            priceCrawlerTaskService.listCrawlerTasksByUser().then(
                function(tasks){
                    delete vm.loadingTasks;
                    vm.tasks = tasks;
                },
                function(reason){
                    delete vm.loadingTasks;
                    vm.loadingError = true;
                })
        }

        loadCrawlerTasks();
    }
})();