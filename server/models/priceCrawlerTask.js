const mongoose = require('mongoose');

module.exports =  function(){

    var priceCrawlerSchema = mongoose.Schema({
        user: { type: mongoose.Schema.ObjectId, ref: 'User' },
        name: String,
        url: String,
        crawlerType: String,
        interval: String,
        lastCrawling: Date,
        notifyLowerPrice: Boolean,
        history: [
            {
                data: mongoose.Schema.Types.Mixed,
                date: Date
            }
        ]
    });

    return mongoose.model('PriceCrawlerTask', priceCrawlerSchema);
};
