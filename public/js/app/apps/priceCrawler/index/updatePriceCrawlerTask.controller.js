(function () {
    angular
        .module('lifeapps')
        .controller('UpdatePriceCrawlerTaskController', Controller);
    Controller.$inject = ['$mdDialog', 'priceCrawlerTaskService', 'priceCrawlerSettings', 'notifier', 'task'];
    function Controller($mdDialog, priceCrawlerTaskService, priceCrawlerSettings, notifier, task) {
        var vm = this;

        vm.intervals = priceCrawlerSettings.intervals;
        vm.crawlers = priceCrawlerSettings.crawlers;

        console.log(task);

        vm.taskData = angular.copy(task);
        delete vm.taskData._id;
        delete vm.taskData.history;
        delete vm.taskData.user;
        delete vm.taskData.lastCrawling;
        
        vm.cancel = function(){
            $mdDialog.cancel();
        };
        
        vm.update = function(){
            if (vm.updateTaskForm.$pristine){
                notifier.error('index.errors.fillFields');
            }
            else if (vm.updateTaskForm.$valid){
                priceCrawlerTaskService.updateCrawlerTask(task._id, vm.taskData).then(
                    function () {
                        notifier.success('apps.priceCrawler.updateTaskSuccess');
                        $mdDialog.hide();
                    },
                    function(reason){
                        notifier.error(reason);
                    }
                );
            }
        }
    }
})();