(function() {
    angular
        .module('lifeapps')
        .config(Config);
    Config.$inject = ['$mdThemingProvider'];
    function Config($mdThemingProvider) {
        $mdThemingProvider.theme('default')
            .primaryPalette('yellow')
            .accentPalette('amber')
            .warnPalette('red')
            .dark();

        $mdThemingProvider.theme('highlight')
            .primaryPalette('amber')
            .accentPalette('grey')
            .warnPalette('red')
            .backgroundPalette('grey');

        $mdThemingProvider.theme('success-toast');
        $mdThemingProvider.theme('error-toast');
        $mdThemingProvider.theme('warning-toast');
        $mdThemingProvider.theme('info-toast');

    }
})();