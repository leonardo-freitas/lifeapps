(function () {
    angular
        .module('lifeapps')
        .controller('NewPriceCrawlerTaskController', Controller);
    Controller.$inject = ['$mdDialog', 'priceCrawlerTaskService', 'priceCrawlerSettings', 'notifier', 'identityService'];
    function Controller($mdDialog, priceCrawlerTaskService, priceCrawlerSettings, notifier, identityService) {
        var vm = this;

        vm.intervals = priceCrawlerSettings.intervals;
        vm.crawlers = priceCrawlerSettings.crawlers;
        
        vm.cancel = function(){
            $mdDialog.cancel();
        };
        
        vm.create = function(){
            if (vm.newTaskForm.$pristine){
                notifier.error('index.errors.fillFields');
            }
            else if (vm.newTaskForm.$valid){
                var newTask = {
                    user: identityService.currentUser._id,
                    name: vm.name,
                    url: vm.url,
                    crawlerType: vm.crawlerType,
                    interval: vm.interval,
                    notifyLowerPrice: vm.notifyLowerPrice
                };
                priceCrawlerTaskService.createCrawlerTask(newTask).then(
                    function () {
                        notifier.success('apps.priceCrawler.newTaskSuccess');
                        $mdDialog.hide();
                    },
                    function(reason){
                        notifier.error(reason);
                    }
                );
            }
        }
    }
})();