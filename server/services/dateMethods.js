module.exports = {
    minutesDiff: function(dateA, dateB){
        var diffMs = Math.abs(dateB - dateA);
        return Math.round(diffMs / 60000);
    }
};