module.exports = function (app) {
    
    var priceCrawlerController = app.controllers.priceCrawlerController;

    app.route('/api/priceCrawlerTask/')
        .post(priceCrawlerController.createCrawlerTask);
    
    app.route('/api/priceCrawlerTask/:id')
        .get(priceCrawlerController.listCrawlerTasksByUser)
        .post(priceCrawlerController.updateCrawlerTask)
        .delete(priceCrawlerController.deleteCrawlerTask);

};
