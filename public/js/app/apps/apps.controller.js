(function () {
    angular
        .module('lifeapps')
        .controller('AppsController', Controller);
    Controller.$inject = ['appsSettings'];
    function Controller(appsSettings) {
        var vm = this;
        vm.apps = appsSettings.apps;
    }
})();