(function() {
    angular
        .module('lifeapps')
        .config(TranslateConfig);
    TranslateConfig.$inject = ['$translateProvider', 'translatePTBR'];
    function TranslateConfig($translateProvider, translatePTBR) {
        $translateProvider.translations('ptbr', translatePTBR);
    }
})();
