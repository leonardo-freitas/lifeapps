

module.exports = function(app){

    var PriceCrawlerTask = app.models.priceCrawlerTask;

    var controller = {};

    controller.createCrawlerTask = function(req, res){
        var taskData = req.body;

        PriceCrawlerTask.create(taskData, function(err){
            if (err){
                res.send({success: false, error: err, reason: "common.connectionError"});
            }
            else{
                res.send({success: true});
            }
        });
    };
    
    controller.updateCrawlerTask = function(req, res){
        var taskData = req.body;

        PriceCrawlerTask.update({_id: req.params.id}, taskData, function(err){
            if (err){
                res.send({success: false, error: err, reason: "common.connectionError"});
            }
            else{
                res.send({success: true});
            }
        });
    };
    
    controller.deleteCrawlerTask = function(req, res){
        PriceCrawlerTask.remove({_id: req.params.id}).exec(function(err){
            if (err){
                res.send({success: false, error: err, reason: "common.connectionError"});
            }
            else{
                res.send({success: true});
            }
        });
    };

    controller.listCrawlerTasksByUser = function(req, res){
        PriceCrawlerTask.find({user: req.params.id}).exec(function(err, docs){
            if (err){
                res.send({success: false, error: err, reason: "common.connectionError"});
            }
            else{
                res.send({success: true, tasks: docs});
            }
        })
    };

    return controller;
};