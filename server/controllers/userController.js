var encryption = require('../services/encryption');
var emailAgent = require('../services/emailAgent');
var i18n = require('../locale/i18n')("ptBR");

module.exports = function(app){

    var User = app.models.user;

    var controller = {};

   controller.updateUser = function(req, res){
        var newUserData = req.body;

       if (newUserData.password){
           newUserData.salt = encryption.createSalt();
           newUserData.password = encryption.hashPwd(newUserData.salt, newUserData.password)
       }

       User.update({_id: req.params.id}, newUserData, function(err){
           if (err){
               res.send({success: false, error: err, reason: "common.connectionError"});
           }
           else{
               res.send({success: true});
           }
       })
   };

   controller.inviteUser = function(req, res){
       var salt = encryption.createSalt();
       var hash = encryption.randomPassword();
       var newUser = {
           email: req.body.email,
           name: req.body.email.substring(0, req.body.email.indexOf("@")),
           salt,
           password: encryption.hashPwd(salt, hash),
           roles: []
       };
       
       User.create(newUser, function(err){
           if (err && err.toString().indexOf('E11000') !== -1){
               res.send({success: false, error: err, reason: "invite.duplicate"});
           }
           else if (err){
               res.send({success: false, error: err, reason: "common.connectionError"});
           }
           else{
               emailAgent.sendEmail(
                   newUser.email,
                   i18n.translate("inviteEmail.subject"),
                   i18n.translate("inviteEmail.html", {token: hash})
               );
               res.send({success: true});
           }
       })
   };

    return controller;
};