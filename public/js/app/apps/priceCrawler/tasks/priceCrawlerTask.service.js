(function() {
    angular
        .module('lifeapps')
        .factory('priceCrawlerTaskService', Service);
    Service.$inject = ['$q', 'identityService',  'PriceCrawlerTask'];
    function Service($q, identityService, PriceCrawlerTask) {
        
        return {
            listCrawlerTasksByUser: listCrawlerTasksByUser,
            createCrawlerTask: createCrawlerTask,
            updateCrawlerTask: updateCrawlerTask,
            deleteCrawlerTask: deleteCrawlerTask
        };

        function listCrawlerTasksByUser() {
            var task = new PriceCrawlerTask();

            var dfd = $q.defer();
            task.$get({id: identityService.currentUser._id}).then(
                function(response){
                    if (response.success){
                        dfd.resolve(response.tasks);
                    }
                    else{
                        dfd.reject(response.reason);
                    }
                },
                function(){
                    dfd.reject("common.connectionError");
                }
            );

            return dfd.promise;
        }
        
        function createCrawlerTask(taskData) {
            var task = new PriceCrawlerTask(taskData);

            var dfd = $q.defer();
            task.$save().then(
                function(response){
                    if (response.success){
                        dfd.resolve();
                    }
                    else{
                        dfd.reject(response.reason);
                    }
                },
                function(){
                    dfd.reject("common.connectionError");
                }
            );

            return dfd.promise;
        }

        function updateCrawlerTask(id, taskData) {
            var task = new PriceCrawlerTask(taskData);

            var dfd = $q.defer();
            task.$save({id: id}).then(
                function(response){
                    if (response.success){
                        dfd.resolve();
                    }
                    else{
                        dfd.reject(response.reason);
                    }
                },
                function(){
                    dfd.reject("common.connectionError");
                }
            );

            return dfd.promise;
        }
        
        function deleteCrawlerTask(id){
            var task = new PriceCrawlerTask();

            var dfd = $q.defer();
            task.$delete({id: id}).then(
                function(response){
                    if (response.success){
                        dfd.resolve();
                    }
                    else{
                        dfd.reject(response.reason);
                    }
                },
                function(){
                    dfd.reject("common.connectionError");
                }
            );

            return dfd.promise;
        }
    }
})();
