function cut(str, cutStart, cutEnd){
    return str.substr(0,cutStart) + str.substr(cutEnd+1);
}

module.exports = {
    priceToNumber: function(price){
        if (!price){
            return null;
        }
        var priceString = price;
        if (price.indexOf("R$") != "-1"){
            priceString = cut(price, price.indexOf("R$"), price.indexOf("R$") + 2);
        }
        priceString = priceString.replace(/\./g, "");
        priceString = priceString.replace(",", ".");
        return parseFloat(priceString);
    }
};
