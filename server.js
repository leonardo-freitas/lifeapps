var http = require('http');

const env = process.env.NODE_ENV || 'development';
const config = require('./config/config')[env];

var app = require('./config/express')(config);
require('./config/database')(app, config);
require('./config/passport')();


http.createServer(app).listen(config.port, config.ip, () => {
	console.log('LifeApps server is running in port localhost:%d', config.port);
});