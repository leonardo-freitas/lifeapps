var priceCrawler = require('./priceCrawler');

var crawlerTypesMethods = {
    "kabum": priceCrawler.kabumScrapper,
    "americanas": priceCrawler.americanasSubmarinoScrapper,
    "submarino": priceCrawler.americanasSubmarinoScrapper,
    "extra": priceCrawler.GPAScrapper,
    "pontofrio": priceCrawler.GPAScrapper
};

const stack = {
    settings: {
        maxCrawlers: 1,
        updateInterval: 1000
    },
    running: 0,
    queue: []
};

var stackManagerInterval = setInterval(stackManager, stack.settings.updateInterval);

module.exports = {
    push: function(url, crawlerType, callback){
        stack.queue.push({
            method: crawlerTypesMethods[crawlerType],
            params: {url, callback}
        });
    }
};

function stackManager(){
    while(stack.running < stack.settings.maxCrawlers && stack.queue.length){
        var next = stack.queue.splice(0, 1)[0];
        function executeCrawler(crawler){
            stack.running++;
            crawler.method(crawler.params.url, function stackFinished(error, prices){
                stack.running--;
                crawler.params.callback(error, prices);
            });
        }
        executeCrawler(next);
    }
}