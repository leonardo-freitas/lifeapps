(function () {
    angular
        .module('lifeapps')
        .constant('priceCrawlerSettings', {
            crawlers: [
                {
                    id: "kabum",
                    name: "Kabum"
                },
                {
                    id: "americanas",
                    name: "Americanas"
                },
                {
                    id: "submarino",
                    name: "Submarino"
                },
                {
                    id: "pontofrio",
                    name: "Ponto Frio"
                },
                {
                    id: "extra",
                    name: "Extra"
                }
            ],
            intervals: [
                {
                    id: "5m",
                    name: "apps.priceCrawler.intervals.fiveMinutes"
                },
                {
                    id: "10m",
                    name: "apps.priceCrawler.intervals.tenMinutes"
                },
                {
                    id: "30m",
                    name: "apps.priceCrawler.intervals.halfHour"
                },
                {
                    id: "1h",
                    name: "apps.priceCrawler.intervals.oneHour"
                },
                {
                    id: "2h",
                    name: "apps.priceCrawler.intervals.twoHours"
                },
                {
                    id: "12h",
                    name: "apps.priceCrawler.intervals.halfDay"
                },
                {
                    id: "1d",
                    name: "apps.priceCrawler.intervals.oneDay"
                },
                {
                    id: "1w",
                    name: "apps.priceCrawler.intervals.oneWeek"
                }
            ]
        });
})();